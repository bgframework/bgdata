
.. _repo:

Repositories
============

.. |r| replace:: *remote*
.. |l| replace:: *local*
.. |c| replace:: *cache*


|bgd| manages the packages through 3 layers of repositories:

- *remote*
- *local*
- *caches*


Remote
------

The |r| represents a repository that serves as a source of data packages.
Currently, it is an HTTP server that contains the compressed data packages
and some tags.

When the user requests for a package that is not present in the |l| repository
|bgd| will download it from the |r| into the local.

In addition, |bgd| will keep in sync the ``tags``.
This means that if a |tg| of a particular package
is updated in the |r|, and the user requests that
particular |tg|, he or she will get the latest version
from the |r| if the local |tg| was not up to date.


.. note:: |bgd| can work in offline mode.
   In such case, packages will not be downloaded and
   tags will not be updated.

Local
-----

The local repository is the one where the user can find the packages
that have been requested.

While the remote is an HTTP server, the local should be a reachable path
from the user's machine.

The main difference with the |r| repository,
apart from being in the local machine,
is that packages are uncompressed.

The download process
********************

The download process from the remote is done using
the Python package `homura <https://github.com/shichao-an/homura>`_.
Thanks to it, downloads can be resumed.
After download, |bgd| extracts all the files if they were compressed.

Once the download and extraction processes are done |bgd| creates
a file named :file:`.download` with the date and time of that moment.
If this file is not present or deleted, |bgd| assumes the
download has failed and reattempts it.

Caches
------

A |c| is an extension of the local repository.
Like the local repository, it should be reachable path from the user's machine.
Moreover, |bgd| supports multiple caches.

When the user request a packages, |bgd| will be search for it
first in each |c| and the in the |l| repository.

A |c| can have different uses.
As an example, we use the scratch space in the nodes of our cluster
to as cache for the packages we use recurrently.
For the others, we have a |l| repository reachable through the network
file system.

.. important:: |bgd| will not fail just because a |c| is not present.
   This means that you can also use an external hard drive as
   a |c| and if it is not connected |bgd| can still be used.

