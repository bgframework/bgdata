.. bgdata documentation master file, created by
   sphinx-quickstart on Thu Feb  8 09:30:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

bgdata
======

|bgd| is a simple :ref:`data package <package>` manager.
It allows to create, search and use :ref:`data packages <package>`.

By default, it works with the data packages used by our group:
`Barcelona Biomedical Genomics Group <https://bbglab.irbbarcelona.org/>`_.

|bgd| downloads once the packages from a remote :ref:`repository <repo>`
and keeps them in a local repository (typically a local folder)
so that it is fast to access them.
To get a better overview of how the repositories work
check the :ref:`repositories <repo>` section.

However, |bgd| is more than that, keep reading to find out what it can do.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   package
   repo
   configuration
   usage
   advanced



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
