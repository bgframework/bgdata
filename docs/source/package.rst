
.. _package:

The data packages
=================


A data package is nothing more, and nothing less, that set of files
(or even a single file).


Identifying
-----------


|bgd| identifies each data package with a 4-level structure

1. *project*
#. *dataset*
#. *version*
#. *build*

``project`` and ``dataset`` are the *main identifiers* of a data package.
In some cases, you might find that the package does not belong to
a particular project. For such cases, we use ``_`` as project name.
Some of the |bgd| commands will automatically set the project as
``_`` if you do not provide it.

The ``version`` is intended distinguish between *incompatible*
versions of the package. E.g. when you are removing some data columns in your files.

The |bld| is an identifier that allows to distinguish between *compatible* versions
of the same packages. Typically, we use the date when we create the package as
the |bld| identifier. However, the |bld| can be anything (as long as it does not
start with an alpha character), so you might find other builds.


For example, we use the human genome in many of projects.
There are several version of the human genome available at http://hgdownload.cse.ucsc.edu/downloads.html#human .
We downloaded our data of interest for the hg19 version and created packages
using ``_`` as *project*, ``genomereference`` as *dataset*,
``hg19`` as *version* and ``20150724`` as *build*.
Then, we can request this package as

.. code-block:: bash

   bgdata get _/genomereference/hg19?20150724

Tags
----

As remembering all the |bld| identifiers for all the packages might be painful
and you probably need to change all the queries in your scripts to get
newer versions, |bgd| supports the concept of **tags**.

A |tg| is a pointer to a particular build, and in several
operations with |bgd| you can use a |tg| instead of a |bld|.
|bgd| will resolve which is the |bld| associated with that |tg|
and use that package.

The advantage of using a |tg| rather than a |bld| is that
with the same query in your software, you get the most updated
version of a particular package by only keeping the |tg| up to date.
E.g. following the example above, if we ask for the |tg| *master*
we get the always the most recent version:

.. code-block:: bash

   bgdata get _/genomereference/hg19?master

provided that we keep our |tg| up to date.

In most cases, |bgd| will use the |tg| *master*
when you do not indicate the |bld| or |tg| for a particular package.

.. important:: A |tg| works essentially as a pointer to a |bld|
   for a particular ``project``, ``dataset`` and ``version``.
   This means that when asking for a |tg| you also need to
   indicate the other parameters.

