
.. _config:

Configuring bgdata
==================

|bgd| has a default configuration file which looks like::

    version=2
    local_repository = "~/.bgdata"
    remote_repository = "http://bbglab.irbbarcelona.org/bgdata"

However, you can create you own configuration file and change it.

Custom configuration
--------------------

To create you own custom configuration you need to create a
file :file:`bgdatav2.conf` and place in the corresponding
config file folder (this is done using the
`appdir package <https://pypi.org/project/appdirs/>`_
using the ``user_config_dir`` function with ``bbglab``
as the only parameter).

That file, should follow the same structure as the default,
but you can change the sections to fit you own needs.

----

The **local folder** (where the data packages are stored)
is indicated through ``local_repository``.


.. literalinclude:: bgdata.example.conf
   :language: text
   :lines: 2-3

.. note:: You can put any reachable path.

----

The **remote repository** is a (public) URL where the data packages
are stored and the |bgd| uses to look for the packages that
are not in the local repository.

.. literalinclude:: bgdata.example.conf
   :language: text
   :lines: 5-6


----

If you need to access to the remote repo through a proxy
you can also configure it as follows:

.. literalinclude:: bgdata.example.conf
   :language: text
   :lines: 11-19

----

Optionally, |bgd| can be set to **not** look for newer versions of the packages
in the remote repository and only use what is available on the local.
To make use of this option, you need to add:

.. literalinclude:: bgdata.example.conf
   :language: text
   :lines: 8-9


----

Using the ``cache_repositories`` option you can indicate
a list of repositories (similar to the local) in which
to look for the files.

.. literalinclude:: bgdata.example.conf
   :language: text
   :lines: 20-23

.. note:: cache repositories have higher priority than
   the local, meaning that |bgd| will look in them
   before checking the local. In addition,
   they are search last to first.

As an example of usage, data packages that are being used recurrently
in our cluster are saved in the ``scratch`` directory of
each node. This way, |bgd| takes the data from the ``scratch``
which is faster than using the network file system.
