from configobj import ConfigObj, interpolation_engines
from bgdata.configobj import BgDataInterpolation
from os.path import dirname, join

interpolation_engines['bgdata'] = BgDataInterpolation
config = ConfigObj(join(dirname(__file__), 'bgdataconf.conf'), interpolation='bgdata')
print(config)


