local_repository = string(default='~/.bgdata')
remote_repository = string(default='http://bg.upf.edu/bgdata')
cache_repositories = force_list(default=list())
offline = boolean(default=False)